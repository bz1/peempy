0.2.0
=====

Initial release version

0.2.1
=====

* Various improvement for the command line interface
* Various bug fixes
* Changed the save data type to be single precision floating point for XMCD signals
* Re-enabled normalisation of the frames
* Added gaussian filter for correction to counter stational error signals 
* Updated the 2D fitting function for 2D vector maps


