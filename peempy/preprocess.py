"""
A Module that contains pre-procssing routines
"""

import numpy as np
from .imgstack import ImageStack
from enum import Enum


class IncompatibleError(RuntimeError):
    """Error for incompatible capture type with the operation"""
    pass


class CaptureTypes(Enum):
    """
    CaptureTypes Enum
    """
    PCO = 'PCOImage'
    UV = 'UViewImage'
    MEDI = 'medipixImage'


def suffix2type(suffix):
    """
    Convert suffix to CaptureTypes
    """

    for enum in CaptureTypes:
        if suffix == enum.value:
            return enum
    raise ValueError('CaptureType not found for suffix <{}>'.format(suffix))


class PreProcessor(ImageStack):
    """
    A class for precessing of the images
    """
    SUFFIX = None

    def __init__(self, images, capture_type=None, suffix=None, inplace=False):
        """
        Instanciate the PreProcessor
        """
        if suffix is not None:
            capture_type = suffix2type(suffix)

        if capture_type is None:
            raise ValueError(
                'capture_type is not defined and cannot be inferred')

        if capture_type == CaptureTypes.MEDI:
            type_cast = np.float32
        else:
            type_cast = None

        super(PreProcessor, self).__init__(images, type_cast=type_cast)
        self.processed_frames = None
        self.inplace = inplace
        self.capture_type = capture_type
        self._background_substracted = False
        self._has_processed = False

    def pre_process(self):
        """
        Conduct the Pre-processing
        """
        if self._has_processed is True:
            raise RuntimeError('Pre-processing has been completed')

        if self.capture_type in [CaptureTypes.PCO, CaptureTypes.UV]:
            self._substract_counts()
            self._has_processed = True

        elif self.capture_type == CaptureTypes.MEDI:
            self._fix_bad_pixels()
            frames = self._fix_cross()
            outframes = self._expand_cross(frames)
            # For consistent we swap the initial data is inplace is requested
            if self.inplace:
                self.frames = outframes

            self.processed_frames = outframes
            self._has_processed = True

        return self.processed_frames

    def _fix_bad_pixels(self):
        """
        Fix bad pixels by swaping from left to right and assign any
        missing pixels to the last slice
        """
        dead_pixels = np.all((self.frames == 0), axis=0)
        frames = self.frames if self.inplace else self.frames.copy()

        # Implement a simple method that treats the dead pixels
        last_slice = None
        for dead_pixel_line, image_slice in zip(dead_pixels.T,
                                                np.swapaxes(frames, 0, 2)):
            if last_slice is not None:
                # Assign the dead pixels to that of the previous slice
                image_slice[dead_pixel_line, :] = last_slice[
                    dead_pixel_line, :]
            last_slice = image_slice

        return frames

    def _fix_cross(self):
        """
        Fix the cross of the image in the centre
        """
        npixel = self.frames.shape[1]
        halfpix = npixel // 2
        nframes = self.frames.shape[0]
        frames = self.frames if self.inplace else self.frames.copy()
        # Mask for the cross
        mask = np.zeros(self.frames.shape[1:], dtype=np.bool)
        mask[halfpix - 1:halfpix + 1, :] = True
        frames[:, mask] = frames[:, mask] / 2
        # Reset the mask
        mask[:, :] = False
        mask[:, halfpix - 1:halfpix + 1] = True
        frames[:, mask] = frames[:, mask] / 2
        return frames

    @staticmethod
    def _expand_cross(frames):

        npixel = frames.shape[1]
        halfpix = npixel // 2
        nframes = frames.shape[0]
        # Now expand by 2 lines
        outframe = np.zeros((nframes, npixel + 2, npixel + 2),
                            dtype=frames.dtype)
        # Top left quarter
        outframe[:, :halfpix, :halfpix] = frames[:, :npixel // 2, :halfpix]
        # Top right quarter
        outframe[:, :halfpix, halfpix + 2:] = frames[:, :npixel // 2, halfpix:]
        # Bottom left quarter
        outframe[:, halfpix + 2:, :halfpix] = frames[:, npixel // 2:, :halfpix]
        # Bottom right quarter
        outframe[:, halfpix + 2:, halfpix + 2:] = frames[:, halfpix:, halfpix:]

        # Copy the missing lines
        outframe[:, halfpix, :] = outframe[:, halfpix - 1, :]
        outframe[:, :, halfpix] = outframe[:, :, halfpix - 1]
        outframe[:, halfpix + 1, :] = outframe[:, halfpix + 2, :]
        outframe[:, :, halfpix + 1] = outframe[:, :, halfpix + 2]

        # Fix the centre square
        outframe[:, halfpix, npixel // 2] = outframe[:, halfpix - 1,
                                                     halfpix - 1]  # Top left
        outframe[:, halfpix + 1, npixel //
                 2] = outframe[:, halfpix + 2, halfpix - 1]  # Bottom left
        outframe[:, halfpix, halfpix + 1] = outframe[:, halfpix - 1,
                                                     halfpix + 2]  # Top right
        outframe[:, halfpix + 1, halfpix +
                 1] = outframe[:, halfpix + 2, halfpix + 2]  # Bottom right

        return outframe

    def _substract_counts(self):
        """
        Substract the count by 100
        only make sense for PCOImage and UViewImage
        """
        if self.capture_type not in [CaptureTypes.PCO, CaptureTypes.UV]:
            raise IncompatibleError('Substract will lead to underflow')

        if self._background_substracted is True and self.inplace:
            raise RuntimeError('Background already substracted')

        frames = self.frames if self.inplace else self.frames.copy()
        if frames.min() < 100:
            # Avoid underflow by assigning any pixel < 100 to 100
            mask = frames < 100
            frames[mask] = 100

        frames -= 100
        self._background_substracted = True
        self.processed_frames = frames
        return frames
