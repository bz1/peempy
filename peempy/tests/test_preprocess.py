"""
Test for the preprocessor
"""

import numpy as np
import pytest
from peempy.preprocess import PreProcessor, CaptureTypes, IncompatibleError


@pytest.fixture
def processor():
    data = np.arange(1, 17, dtype=np.uint32).reshape((1, 4, 4))
    return PreProcessor(data, capture_type=CaptureTypes.MEDI)


@pytest.fixture
def processor_with_cross(processor):
    processor.frames[:, 1, :] = processor.frames[:, 1, :] * 2
    processor.frames[:, 2, :] = processor.frames[:, 2, :] * 2
    processor.frames[:, :, 1] = processor.frames[:, :, 1] * 2
    processor.frames[:, :, 2] = processor.frames[:, :, 2] * 2
    return processor


def test_processor_init():
    """Test initialisation of processor objects"""
    data = np.arange(1, 17).reshape((1, 4, 4))
    PreProcessor(data, suffix='PCOImage')
    with pytest.raises(ValueError):
        PreProcessor(data)


def test_fix_zeros(processor):
    """
    Test fixing bad pixels
    """

    processor.frames[0, 1, 1] = 0
    frames = processor._fix_bad_pixels()
    assert frames[0, 1, 1] == 5


def test_fix_cross(processor_with_cross, processor):
    """Test heling the cross in the centre"""
    processor_with_cross._fix_cross()
    assert np.all(processor_with_cross.frames == processor.frames)


def test_expand_cross(processor):
    frames = processor.frames
    outframes = processor._expand_cross(frames)
    assert outframes.shape == (1, 6, 6)
    assert np.all(outframes[:, :2, :2] == frames[:, :2, :2])
    assert np.all(outframes[:, 3, 3] != 0)


def test_substract_background(processor):
    """Test background substraction"""
    init_frames = processor.frames.copy()
    with pytest.raises(IncompatibleError):
        processor._substract_counts()

    processor.capture_type = CaptureTypes.PCO
    frames = processor._substract_counts()
    assert np.all(frames == 0)

    processor.frames += 100
    frames = processor._substract_counts()
    assert np.all(frames == init_frames)


def test_process_medi(processor_with_cross):
    """Test processing MedipixImage"""
    processor = processor_with_cross
    processor.pre_process()
    assert np.all(processor.processed_frames != 0)
    assert processor.frames.shape[1] == processor.processed_frames.shape[1] - 2

    with pytest.raises(RuntimeError):
        processor.pre_process()


def test_suffix2type():
    """Test suffix2type convertion function"""
    from peempy.preprocess import suffix2type
    assert suffix2type('medipixImage') == CaptureTypes.MEDI
