# -*- coding: utf-8 -*-
import os
import skimage.io as io
import numpy as np
import matplotlib.pyplot as plt
dfolder = r"X:\data\2017\si16400-1\processing\tif"
from peemproc.imageproc import get_circular_mask, DriftCorrector, ImageSeries, show_images_series

#%% Fuctions


def norm_mask(array, mask):
    """Normalise propertly using maximum and minimum"""
    array = array.copy()
    if np.ndim(array) == 3:
        array[:, mask] -= array[:, mask].min()
        array[:, mask] = array[:, mask] / array[:, mask].max()
    else:
        array[mask] /= array[mask] - array[mask].min()
        array[mask] = array[mask] / array[mask].max()
    return array


#%%
# This are the images for region 1
fe = io.imread(os.path.join(dfolder, "I_I_150135_143.tif"))
fe_mag = io.imread(os.path.join(dfolder, "I_D_150135_143.tif"))
ni = io.imread(os.path.join(dfolder, "I_I_150150_158.tif"))
ni_mag = io.imread(os.path.join(dfolder, "I_D_150150_158.tif"))
ims = ImageSeries([fe, ni])
ims_mag = ImageSeries([fe_mag, ni_mag])
ims.manual_crop()
#%%
ims.update_crop()
dc = DriftCorrector(ims.frames, 0, ims.drift_crop)
dc_mag = DriftCorrector(ims_mag.frames, 0, ims.drift_crop)
dc.correct()
#%% Now extract images and clip them
# Apply drift correction to magnetic signals
dc_mag.drifts = dc.drifts
dc_mag.apply_drift_corr()
#%% Normalise data
aligned = dc.cor_frames.copy()
aligned_mag = dc_mag.cor_frames.copy()
c_mask = get_circular_mask(fe, 0.8)
aligned = aligned * c_mask
aligned_mag = aligned_mag * c_mask
aligned = norm_mask(aligned, c_mask)
aligned_mag = norm_mask(aligned_mag, c_mask)
show_images_series(aligned, True)
#%% Now do normalisation
aligned[0] = aligned[0] / np.mean(aligned[0][c_mask])
aligned[1] = aligned[1] / np.mean(aligned[1][c_mask])
show_images_series(aligned, False)
#%% apply masks
fe_mask = aligned[0] > 1.1
plt.figure()
plt.imshow(fe_mask)
plt.figure()
plt.imshow(aligned[0])
#%%
ni_mask = aligned[1] > 0.85
plt.figure()
plt.imshow(ni_mask)
plt.figure()
plt.imshow(aligned[1])
#%% Substract Ni by Fe
plt.figure()
b = np.zeros(ni_mask.shape)
plt.imshow(np.stack((fe_mask, ni_mask, b), axis=-1))
plt.figure()
plt.imshow(np.stack((aligned[0], aligned[1], b), axis=-1))
#%% do a difference image
ni_minus_fe = aligned[1] - aligned[0]
io.imshow(ni_minus_fe)
#%% Use this as a mask for Ni image
mask = ni_minus_fe > 0.5
nio = aligned[1] * mask
plt.figure()
plt.imshow(nio)
